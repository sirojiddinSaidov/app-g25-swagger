package uz.pdp.apphateaos.controller;

import ch.qos.logback.core.model.INamedModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.*;
import uz.pdp.apphateaos.entity.Customer;
import uz.pdp.apphateaos.payload.PaginationDTO;
import uz.pdp.apphateaos.repository.CustomerRepository;

import java.util.List;
import java.util.function.Supplier;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerRepository customerRepository;
    static final String ONE_PATH = "/one";

    @Operation(summary = "Add customers here")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the book",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Customer.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Customer not found",
                    content = @Content)})
    @PostMapping
    public Customer add(@RequestBody Customer customer) {
        if (customerRepository.existsByEmail(customer.getEmail())) {
            throw new RuntimeException();
        }
        customerRepository.save(customer);
        return customer;
    }


    @GetMapping
    public PaginationDTO<List<Customer>> list(@RequestParam(defaultValue = "0") int page,
                                              @RequestParam(defaultValue = "10") int size) {

//        Pageable pageable = PageRequest.of(page, size);
//        Sort.Order order = Sort.Order.desc("name");
//        Sort.Order order2 = Sort.Order.desc("email");
//        Sort sort = Sort.by(Sort.Direction.DESC, "name", "email");//order by name, email

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "name", "email");
        Page<Customer> customerPage = customerRepository.findAll(pageable);
        long totalElements = customerPage.getTotalElements();
        List<Customer> customers = customerPage.getContent();
        int totalPages = customerPage.getTotalPages();
        for (Customer customer : customers) {
            Link selfLink = linkTo(CustomerController.class)
                    .slash(ONE_PATH)
                    .slash(customer.getId())
                    .withSelfRel();
            customer.add(selfLink);
        }

        return new PaginationDTO<>(customers, page, size, totalElements, totalPages);
    }


    @GetMapping("/test")
    public PaginationDTO<List<Customer>> list2(@ParameterObject Pageable pageable) {

        Page<Customer> customerPage = customerRepository.findAll(pageable);
        long totalElements = customerPage.getTotalElements();
        List<Customer> customers = customerPage.getContent();
        int totalPages = customerPage.getTotalPages();
        for (Customer customer : customers) {
            Link selfLink = linkTo(CustomerController.class)
                    .slash(ONE_PATH)
                    .slash(customer.getId())
                    .withSelfRel();
            customer.add(selfLink);
        }

        return new PaginationDTO<>(customers, pageable.getPageNumber(), pageable.getPageSize(), totalElements, totalPages);
    }


    @GetMapping(ONE_PATH + "/{id}")
    public Customer one(@PathVariable Integer id) {

        Customer customer = customerRepository.findById(id).orElseGet(Customer::new);
        Link selfLink = linkTo(CustomerController.class)
                .slash(customer.getId())
                .withSelfRel();
        customer.add(selfLink);
        return customer;
    }
}
