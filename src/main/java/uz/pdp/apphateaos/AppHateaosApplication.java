package uz.pdp.apphateaos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppHateaosApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppHateaosApplication.class, args);
    }

}
